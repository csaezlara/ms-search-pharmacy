import { Expose } from "class-transformer";


export class PharmacyResponseDTO {
  @Expose({ name: 'local_nombre' }) localName: string;
	@Expose({ name: 'local_direccion' }) address: string;
	@Expose({ name: 'local_telefono' }) phone: string;
  @Expose({ name: 'local_lat' }) lat: number;
  @Expose({ name: 'local_lng' }) lng: number;

  public constructor(init?: Partial<PharmacyResponseDTO>) {
    Object.assign(this, init);
  }

}