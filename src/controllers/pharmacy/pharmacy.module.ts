import { HttpModule, Module } from '@nestjs/common';
import { FarmanetMinsalServiceClient } from 'src/shared/minsal/farmanet.minsal.service.client';
import { PharmacyController } from './pharmacy.controller';
import { PharmacyService } from './pharmacy.service';

@Module({
  imports: [HttpModule.registerAsync({
    useFactory: () => ({
      timeout: 2000
    }),
  })],
  controllers: [PharmacyController],
  providers: [FarmanetMinsalServiceClient, PharmacyService]
})
export class PharmacyModule {}
