import { HttpModule } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { FarmanetMinsalServiceClient } from 'src/shared/minsal/farmanet.minsal.service.client';
import { PharmacyService } from './pharmacy.service';

const jsonResponse = {address: "AVENIDA EL SALTO 2972", lat: "-33.3996351", lng: "-70.62894990000001", localName: "AHUMADA", phone: "+560225053570"};

const farmanetResponse = { fecha: "02-03-2021", local_id: "534", local_nombre: "AHUMADA", comuna_nombre: "RECOLETA", localidad_nombre: "RECOLETA", local_direccion: "AVENIDA EL SALTO 2972", funcionamiento_hora_apertura: "10:30 hrs.", funcionamiento_hora_cierre: "19:30 hrs.", local_telefono: "+560225053570", local_lat: "-33.3996351", local_lng: "-70.62894990000001", funcionamiento_dia: "martes", fk_region: "7", fk_comuna: "122" };

describe('PharmacyService', () => {
  let service: PharmacyService;
  let apiServiceClient: FarmanetMinsalServiceClient;

  beforeEach(async () => {

    const module: TestingModule = await Test.createTestingModule({
      providers: [PharmacyService, FarmanetMinsalServiceClient],
      imports: [HttpModule.registerAsync({
        useFactory: () => ({
          timeout: 2000
        }),
      })],
    }).compile();

    service = module.get<PharmacyService>(PharmacyService);
    apiServiceClient = module.get<FarmanetMinsalServiceClient>(FarmanetMinsalServiceClient);
  });

  describe('onDuty', () => {
    it('should get onDuty', async () => {
      jest.spyOn(apiServiceClient, '_fetchPharmacyByRegionId')
        .mockImplementation(() => Promise.resolve([farmanetResponse]));
      const pharmacies = await service.onDuty('recoleta', 'ahumada');
      expect(pharmacies).toEqual([jsonResponse]);
    });
  });
  
  afterEach(() => jest.clearAllMocks());
});
