import { HttpModule } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { FarmanetMinsalServiceClient } from 'src/shared/minsal/farmanet.minsal.service.client';
import { PharmacyController } from './pharmacy.controller';
import { PharmacyService } from './pharmacy.service';

const jsonResponse = {address: "AVENIDA EL SALTO 2972", lat: "-33.3996351", lng: "-70.62894990000001", localName: "AHUMADA", phone: "+560225053570"};

describe('PharmacyController', () => {
  let controller: PharmacyController;
  let service: PharmacyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PharmacyController],
      providers: [PharmacyService, FarmanetMinsalServiceClient],
      imports: [HttpModule.registerAsync({
        useFactory: () => ({
          timeout: 2000
        }),
      })],
    }).compile();

    controller = module.get<PharmacyController>(PharmacyController);
    service = module.get<PharmacyService>(PharmacyService);
  });

  describe('getByFilter', () => {
    it('should get getByFilter', async () => {
      jest.spyOn(service, 'onDuty')
        .mockImplementation(() => Promise.resolve([jsonResponse]));
      const pharmacies = await controller.getByFilter('recoleta', 'ahumada');
      expect(pharmacies).toEqual([jsonResponse]);
    });
  });
  
  afterEach(() => jest.clearAllMocks());
});