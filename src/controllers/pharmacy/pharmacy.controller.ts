import { Query } from '@nestjs/common';
import { Controller, Get } from '@nestjs/common';
import { PharmacyService } from './pharmacy.service';

@Controller('pharmacy')
export class PharmacyController {

  constructor(private pharmacyService: PharmacyService) {}

  @Get('on-duty')
  getByFilter(
    @Query('commune') commune: string, 
    @Query('localName') localName: string): Promise<any>{
    return this.pharmacyService.onDuty(commune,localName);
  }
}
