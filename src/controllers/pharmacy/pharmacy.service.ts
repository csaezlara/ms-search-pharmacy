import { Injectable, NotFoundException } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { PharmacyResponseDTO } from 'src/dto/pharmacy/pharmacy.response.dto';
import { FarmanetMinsalServiceClient } from 'src/shared/minsal/farmanet.minsal.service.client';

@Injectable()
export class PharmacyService {

  constructor(private farmanetMinsalServiceClient: FarmanetMinsalServiceClient) {}

  async onDuty(commune: string, localName: string): Promise<any> {
    if (commune !== undefined && localName !== undefined) {
      const data = await this.farmanetMinsalServiceClient._fetchPharmacyByRegionId(7);
      const pharmacyData = data.filter(pharmacy => pharmacy.comuna_nombre === commune.toUpperCase() 
          && pharmacy.local_nombre === localName.toUpperCase());
      if (pharmacyData.length !== 0) {
        return pharmacyData
          .map(pharmacy => plainToClass(PharmacyResponseDTO, 
            pharmacy, { excludeExtraneousValues: true }));
      }
    }
    throw new NotFoundException('commune or localName not exists');
  }
}
