import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PharmacyModule } from './controllers/pharmacy/pharmacy.module';

@Module({
  imports: [PharmacyModule],
  providers: [AppService],
})
export class AppModule {}
