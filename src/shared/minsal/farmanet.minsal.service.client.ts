import { HttpService, Injectable } from "@nestjs/common";
import { map } from 'rxjs/operators';


@Injectable()
export class FarmanetMinsalServiceClient {

  constructor(private httpService: HttpService) {}

  public async _fetchPharmacyByRegionId(regionId: number): Promise<any> {
    return  Promise.resolve(this.httpService.get<any>('https://farmanet.minsal.cl/maps/index.php/ws/getLocalesRegion?id_region=' + regionId)
      .pipe(map(response => response.data))
      .toPromise());
  }
}