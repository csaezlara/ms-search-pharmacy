## Description

Este microservicio permite obtener las farmacias de turno según la comuna y el nombre del local. El front de este proyecto se encuentra en: [https://tecnico.azurewebsites.net/](https://tecnico.azurewebsites.net/)

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Running the app with Docker

# build
$ docker-compose build

# run
$ docker-compose up