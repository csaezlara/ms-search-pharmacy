FROM node:12.18.3-alpine as producction

# Create app directory
WORKDIR /usr/src/app
RUN mkdir /usr/src/app/temp

COPY package*.json ./

RUN npm config set registry http://registry.npmjs.org
RUN npm install --only=production

COPY . .

COPY --from=development /usr/src/app/dist ./dist

EXPOSE 3000
CMD ["node", "dist/main"]